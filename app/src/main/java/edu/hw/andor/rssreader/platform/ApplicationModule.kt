package edu.hw.andor.rssreader.platform

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: AndroidApplication) {
	@Provides
	@Singleton
	internal fun provideContext(): Context = application


	@Provides
	@Singleton
	internal fun providePackageManager() = application.packageManager
}
