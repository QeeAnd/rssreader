package edu.hw.andor.rssreader.platform

import android.util.Log
import android.view.ViewManager
import org.jetbrains.anko.AnkoViewDslMarker
import org.jetbrains.anko.custom.ankoView
import org.sufficientlysecure.htmltextview.HtmlTextView

fun <T> T.log(tag: String = "My") = apply { Log.i(tag, this.toString()) }

fun <T : Collection<*>> T?.isNullOrEmpty() = this == null || this.isEmpty()

inline fun ViewManager.htmlTextView(init: (@AnkoViewDslMarker HtmlTextView).() -> Unit): HtmlTextView {
	return ankoView({ ctx -> HtmlTextView(ctx) }, theme = 0) {
		init()
	}
}