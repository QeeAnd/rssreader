package edu.hw.andor.rssreader.ui.detail

import android.arch.lifecycle.Observer
import android.graphics.Color
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.View
import edu.hw.andor.rssreader.domain.entities.Item
import edu.hw.andor.rssreader.ui.browser.CustomTabsManager
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

class DetailUI(private val viewModel: DetailViewModel, private val browser: CustomTabsManager) : AnkoComponent<DetailActivity> {

    companion object {
        const val ID_TITlE2: Int = 2
    }

    override fun createView(ui: AnkoContext<DetailActivity>): View = with(ui) {
        relativeLayout {
            fitsSystemWindows = true
            setBackgroundColor(Color.WHITE)
            verticalLayout {
                padding = dip(30)
                id = ID_TITlE2
                textView {
                    padding = dip(40)
                    text = owner.intent.getStringExtra(DetailActivity.CHANNEL_NAME)
                    setTextColor(Color.BLACK)
                    textSize = 25f
                    gravity = Gravity.CENTER_HORIZONTAL
                }.lparams(matchParent, wrapContent) {
                    topMargin = dip(10)
                }
                textView {
                    setBackgroundColor(Color.RED)
                    textSize = 1f
                }.lparams(matchParent, wrapContent)
            }.lparams(matchParent, wrapContent)

            recyclerView {
                val adp = DetailAdapter(viewModel, browser)
                viewModel.items?.observe(owner, Observer<List<Item>> {
                    if (it != null)
                        adp.swap(it)
                })
                layoutManager = LinearLayoutManager(ctx)
                itemAnimator = DefaultItemAnimator()
                adapter = adp
                isVerticalScrollBarEnabled = true
                addItemDecoration(DividerItemDecoration(ctx, DividerItemDecoration.VERTICAL))
            }.lparams(matchParent, dip(800)) {
                below(ID_TITlE2)
            }
        }
    }
}