package edu.hw.andor.rssreader.injection

import android.content.Context
import android.content.pm.PackageManager
import dagger.Component
import edu.hw.andor.rssreader.api.ApiModule
import edu.hw.andor.rssreader.domain.DomainModule
import edu.hw.andor.rssreader.domain.db.ChannelsDao
import edu.hw.andor.rssreader.domain.db.ItemsDao
import edu.hw.andor.rssreader.platform.ApplicationModule
import edu.hw.andor.rssreader.repository.FeedRepository
import edu.hw.andor.rssreader.ui.detail.DetailViewModel
import edu.hw.andor.rssreader.ui.main.MainViewModel
import okhttp3.OkHttpClient
import java.util.concurrent.Executor
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class), (ApiModule::class), (DomainModule::class)])
interface ApplicationComponent {

    fun inject(into: MainViewModel)

    fun inject(into: DetailViewModel)

    fun provideContext(): Context

    fun providePackageManager(): PackageManager

    fun provideOkHttpClient(): OkHttpClient

    fun provideChannelsDao(): ChannelsDao

    fun provideItemsDao(): ItemsDao

    fun provideExecutor(): Executor

    fun provideFeedRepository(): FeedRepository
}
