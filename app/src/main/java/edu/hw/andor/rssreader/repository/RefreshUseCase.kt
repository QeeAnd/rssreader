package edu.hw.andor.rssreader.repository

import edu.hw.andor.rssreader.api.services.FeedService
import edu.hw.andor.rssreader.domain.db.ChannelsDao
import edu.hw.andor.rssreader.domain.db.ItemsDao
import edu.hw.andor.rssreader.domain.mappers.ChannelMapper
import edu.hw.andor.rssreader.domain.mappers.ItemsMapper
import okhttp3.OkHttpClient
import org.xmlpull.v1.XmlPullParserException
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import java.net.UnknownHostException
import java.util.concurrent.Executor
import javax.inject.Inject

class RefreshUseCase
@Inject
constructor(private val client: OkHttpClient,
            private val itemsDao: ItemsDao,
            private val channelsDao: ChannelsDao,
            private val executor: Executor) {
    fun refreshItems(channelLink: String, callback: (NetworkError) -> Unit) {
        executor.execute {
            try {
                val feedService = retrofit2.Retrofit.Builder()
                        .client(client)
                        .addConverterFactory(SimpleXmlConverterFactory.create())
                        .baseUrl(channelLink)
                        .build()
                        .create(FeedService::class.java)

                val response = feedService.getFeed("").execute()
                val feed = response.body()
                if (feed != null) {
                    feed.url = channelLink
                    val channel = ChannelMapper().map(feed)
                    val items = ItemsMapper().map(feed)
                    channelsDao.insert(channel)
                    itemsDao.insert(items)
                }
                callback(NetworkError.SUCCESS)
            } catch (e: Throwable) {
                when (e) {
                    is UnknownHostException -> callback(NetworkError.DISCONNECTED)
                    is IllegalArgumentException -> callback(NetworkError.BAD_URL)
                    is XmlPullParserException -> callback(NetworkError.NOT_A_FEED)
                    else -> callback(NetworkError.UNKNOWN)
                }
            }
        }
    }
}