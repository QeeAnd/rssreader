package edu.hw.andor.rssreader.ui.main

import android.arch.lifecycle.Observer
import android.graphics.Color
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.Gravity
import android.view.View
import edu.hw.andor.rssreader.R
import edu.hw.andor.rssreader.domain.entities.ChannelUiModel
import edu.hw.andor.rssreader.platform.isNullOrEmpty
import edu.hw.andor.rssreader.repository.NetworkError
import org.jetbrains.anko.*
import org.jetbrains.anko.design.floatingActionButton
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout
import java.util.*

class MainUI(private val viewModel: MainViewModel) : AnkoComponent<MainActivity> {

    companion object {
        const val ID_TITlE: Int = 1

    }

    override fun createView(ui: AnkoContext<MainActivity>): View = with(ui) {
        relativeLayout {
            fitsSystemWindows = true
            setBackgroundColor(Color.WHITE)

            verticalLayout {
                padding = dip(30)
                id = ID_TITlE

                textView {
                    text = "RSSReader"
                    setTextColor(Color.RED)
                    textSize = 20f
                    gravity = Gravity.CENTER_HORIZONTAL
                }.lparams(matchParent, wrapContent) {
                    topMargin = dip(10)
                }

                textView {
                    val month = Calendar.getInstance().get(Calendar.MONTH) + 1
                    val date = month.toString() + "月" + Calendar.getInstance().get(Calendar.DAY_OF_MONTH).toString() + "日"
                    text = date
                    textSize = 20f
                    gravity = Gravity.CENTER_HORIZONTAL
                    padding = dip(20)
                }.lparams(matchParent, wrapContent)

                textView {
                    setBackgroundColor(Color.RED)
                    textSize = 1f
                }.lparams(matchParent, wrapContent)
            }.lparams(matchParent, wrapContent)

            swipeRefreshLayout {
                onRefresh {
                    viewModel.refreshItems()
                }

                recyclerView {
                    val adp = MainAdapter(ctx)
                    viewModel.channels.observe(owner, Observer<List<ChannelUiModel>> {
                        if (it != null) {
                            visibility = if (it.isNullOrEmpty()) View.INVISIBLE else View.VISIBLE
                            this@swipeRefreshLayout.isRefreshing = false
                            adp.swap(it)
                        }
                    })
                    viewModel.errors.observe(owner, Observer<NetworkError> {
                        this@swipeRefreshLayout.isRefreshing = false
                        if (it != null)
                            when (it) {
                                NetworkError.SUCCESS -> Unit
                                NetworkError.DISCONNECTED -> toast(R.string.net_error)
                                NetworkError.BAD_URL -> toast(R.string.url_error)
                                NetworkError.NOT_A_FEED -> toast(R.string.feed_error)
                                NetworkError.UNKNOWN -> toast(R.string.unknown_error)
                            }
                    })
                    adapter = adp
                    itemAnimator = DefaultItemAnimator()
                    layoutManager = LinearLayoutManager(ctx)
                    isVerticalScrollBarEnabled = true
                    addItemDecoration(DividerItemDecoration(ctx, DividerItemDecoration.VERTICAL))
                    ItemTouchHelper(object : ItemTouchHelper.Callback() {
                        override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) =
                                makeMovementFlags(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT)

                        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                                            target: RecyclerView.ViewHolder) = false

                        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) =
                                viewModel.deleteChannel((viewHolder as MainAdapter.MainViewHolder).linkKey)
                    }).attachToRecyclerView(this)
                }
            }.lparams {
                below(ID_TITlE)
            }
            floatingActionButton {
                imageResource = R.drawable.ic_add_white_24dp
                onClick {
                    alert("Add Rss Res") {
                        customView {
                            val et = editText()
                            yesButton { viewModel.addChannel(et.text.toString()) }
                        }
                    }.show()
                }
            }.lparams {
                alignParentBottom()
                alignParentRight()
                bottomMargin = dip(32)
                rightMargin = dip(16)
            }
        }
    }
}