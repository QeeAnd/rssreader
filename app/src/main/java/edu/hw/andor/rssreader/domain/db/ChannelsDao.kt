package edu.hw.andor.rssreader.domain.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import edu.hw.andor.rssreader.domain.entities.Channel
import edu.hw.andor.rssreader.domain.entities.ChannelUiModel

@Dao
interface ChannelsDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(channel: Channel)

    @Query("SELECT * FROM channels")
    fun getChannelsSync(): List<Channel>

    @Query("SELECT linkKey, title, description , count FROM channels " +
            "LEFT JOIN (SELECT count(*) AS count, channelKey FROM items WHERE read = 0 GROUP BY channelKey) " +
            "ON linkKey = channelKey")
    fun getChannelList(): LiveData<List<ChannelUiModel>>

    @Query("Delete From channels Where linkKey = :channelLinkKey")
    fun delete(channelLinkKey: String)
}
