package edu.hw.andor.rssreader.ui.detail

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import edu.hw.andor.rssreader.domain.entities.Item
import edu.hw.andor.rssreader.platform.AndroidApplication
import edu.hw.andor.rssreader.repository.FeedRepository
import javax.inject.Inject

data class DetailViewModel(private val app: Application) : AndroidViewModel(app) {

    @Inject
    lateinit var repository: FeedRepository

    var items: LiveData<List<Item>>? = null

    init {
        (app as AndroidApplication).applicationComponent.inject(this)
    }

    fun initialize(channelLink: String) {
        items = repository.getItems(channelLink)
    }

    fun markAsRead(item: Item) = repository.markAsRead(item)
}
