package edu.hw.andor.rssreader.ui.detail

import android.content.Context
import android.text.TextUtils
import android.view.View
import edu.hw.andor.rssreader.R
import edu.hw.andor.rssreader.platform.htmlTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class DetailItemUI : AnkoComponent<Context> {
    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        cardView {
            relativeLayout {
                val dt = textView {
                    id = R.id.dt
                    textAppearance = R.style.TextAppearance_AppCompat_Title
                    textSize = 20f
                }.lparams {
                    marginStart = dip(3)
                    topMargin = dip(8)
                }
                htmlTextView {
                    id = R.id.dd
                    textAppearance = R.style.TextAppearance_AppCompat_Body1
                    textSize = 15f
                    ellipsize = TextUtils.TruncateAt.END
                }.lparams {
                    below(dt)
                    bottomMargin = dip(8)
                    topMargin = dip(8)
                    rightMargin = dip(5)
                    leftMargin = dip(5)
                }
            }
        }
    }
}