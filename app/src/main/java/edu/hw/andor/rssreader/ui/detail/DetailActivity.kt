package edu.hw.andor.rssreader.ui.detail

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import edu.hw.andor.rssreader.platform.log
import edu.hw.andor.rssreader.ui.browser.CustomTabsHelper
import edu.hw.andor.rssreader.ui.browser.CustomTabsManager
import org.jetbrains.anko.setContentView

class DetailActivity : AppCompatActivity() {

    companion object {
        const val CHANNEL_KEY = "edu.hw.Andor.rssReader.channel.key"
        const val CHANNEL_NAME = "edu.hw.Andor.rssReader.channel.name"
    }

    private val browser by lazy {
        CustomTabsManager(this, CustomTabsHelper(this, this.packageManager))
    }

    private val viewModel by lazy(LazyThreadSafetyMode.NONE) {
        ViewModelProviders.of(this).get(DetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

        viewModel.initialize(intent?.getStringExtra(CHANNEL_KEY).log() ?: "")
        DetailUI(viewModel, browser).setContentView(this)
    }

    override fun onResume() {
        super.onResume()
        browser.unbindCustomTabsService()
    }
}
