package edu.hw.andor.rssreader.ui.browser

import android.content.ComponentName
import android.support.customtabs.CustomTabsClient
import android.support.customtabs.CustomTabsServiceConnection

import java.lang.ref.WeakReference

class ServiceConnection(connectionCallback: ServiceConnectionCallback) : CustomTabsServiceConnection() {
    private val callbackWeakReference: WeakReference<ServiceConnectionCallback> = WeakReference(connectionCallback)

    override fun onCustomTabsServiceConnected(name: ComponentName, client: CustomTabsClient) {
        callbackWeakReference.get()?.onServiceConnected(client)
    }

    override fun onServiceDisconnected(name: ComponentName) {
        callbackWeakReference.get()?.onServiceDisconnected()
    }

    interface ServiceConnectionCallback {

        fun onServiceConnected(client: CustomTabsClient)

        fun onServiceDisconnected()
    }
}