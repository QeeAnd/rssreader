package edu.hw.andor.rssreader.repository

import android.arch.lifecycle.LiveData
import edu.hw.andor.rssreader.domain.db.ChannelsDao
import edu.hw.andor.rssreader.domain.db.ItemsDao
import edu.hw.andor.rssreader.domain.entities.ChannelUiModel
import edu.hw.andor.rssreader.domain.entities.Item
import edu.hw.andor.rssreader.domain.entities.Item.Companion.READ
import edu.hw.andor.rssreader.platform.isNullOrEmpty

class FeedRepository
constructor(private val itemsDao: ItemsDao,
            private val channelsDao: ChannelsDao,
            private val useCase: RefreshUseCase,
            private val errorData: ErrorLiveData) {
    fun getChannels(): LiveData<List<ChannelUiModel>> {
        refreshItems()
        return channelsDao.getChannelList()
    }

    fun getItems(channelLink: String) = itemsDao.getAll(channelLink)

    fun getErrors(): LiveData<NetworkError> = errorData

    fun addChannel(channelLink: String) = useCase.refreshItems(channelLink) { errorData.setNetworkError(it) }

    fun deleteChannel(channelLink: String) = channelsDao.delete(channelLink)

    fun refreshItems() {
        var links = channelsDao.getChannelsSync().map { (linkKey) -> linkKey }
        if (links.isNullOrEmpty()) links = addSampleChannels()
        for (link in links)
            useCase.refreshItems(link) { errorData.setNetworkError(it) }
    }

    fun markAsRead(item: Item) {
        item.read = READ
        itemsDao.updateItem(item)
    }

    private fun addSampleChannels() = listOf(
            "http://feed.androidauthority.com/",
            "https://plumz.me/feed/",
            "http://www.uisdc.com/feed/",
            "https://feeds.appinn.com/appinns/",
            "http://www.apprcn.com/feed/",
            "http://songshuhui.net/feed/"
    )
}