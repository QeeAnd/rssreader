package edu.hw.andor.rssreader.repository

enum class NetworkError {
	SUCCESS,
	DISCONNECTED,
	BAD_URL,
	NOT_A_FEED,
	UNKNOWN
}
