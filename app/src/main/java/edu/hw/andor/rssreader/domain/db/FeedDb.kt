package edu.hw.andor.rssreader.domain.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import edu.hw.andor.rssreader.domain.converter.DateConverter
import edu.hw.andor.rssreader.domain.entities.Channel
import edu.hw.andor.rssreader.domain.entities.Item

@Database(entities = [(Channel::class), (Item::class)], version = 1, exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class FeedDb : RoomDatabase() {

    abstract fun channelsDao(): ChannelsDao

    abstract fun itemsDao(): ItemsDao
}
