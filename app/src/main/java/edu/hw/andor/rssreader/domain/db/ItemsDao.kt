package edu.hw.andor.rssreader.domain.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import edu.hw.andor.rssreader.domain.entities.Item

@Dao
interface ItemsDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(items: List<Item>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(item: Item)

    @Update
    fun updateItem(item: Item)

    @Query("SELECT * FROM items WHERE channelKey = :channelKey ORDER BY pubDate DESC")
    fun getAll(channelKey: String): LiveData<List<Item>>
}
