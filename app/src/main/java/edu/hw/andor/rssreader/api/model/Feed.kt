package edu.hw.andor.rssreader.api.model

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "rss", strict = false)
data class Feed(
		@field:Element(name = "channel")
		var channel: FeedChannel? = null,

		var url: String? = null
)