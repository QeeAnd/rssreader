package edu.hw.andor.rssreader.ui.detail

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import edu.hw.andor.rssreader.R
import edu.hw.andor.rssreader.domain.entities.Item
import edu.hw.andor.rssreader.domain.entities.Item.Companion.READ
import edu.hw.andor.rssreader.ui.browser.CustomTabsManager
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.textColorResource
import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter
import org.sufficientlysecure.htmltextview.HtmlTextView


class DetailAdapter(private val viewModel: DetailViewModel, private val browser: CustomTabsManager) : RecyclerView.Adapter<DetailAdapter.DetailViewHolder>() {
    private var items: List<Item> = listOf()

    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) = holder.bind(items[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailViewHolder = DetailViewHolder(DetailItemUI().createView(AnkoContext.create(parent.context)))

    override fun getItemCount(): Int = items.size

    fun swap(items: List<Item>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class DetailViewHolder(private val mView: View) : RecyclerView.ViewHolder(mView) {
        private val dt: TextView = mView.find(R.id.dt)
        private val dd: HtmlTextView = mView.find(R.id.dd)
        fun bind(item: Item) {
            dt.text = item.title
            dd.setHtml(item.description, HtmlHttpImageGetter(dd, null, true))
            if (item.read == READ) {
                dt.textColorResource = R.color.read
                dd.textColorResource = R.color.read
            } else {
                dt.textColorResource = R.color.unread
                dd.textColorResource = R.color.unread
            }
            mView.onClick {
                viewModel.markAsRead(item)
                browser.showContent(item.link)
            }
        }
    }
}