package edu.hw.andor.rssreader.api.services

import edu.hw.andor.rssreader.api.model.Feed
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface FeedService {
	@GET("{feed}")
	fun getFeed(@Path("feed") feed: String): Call<Feed>
}
