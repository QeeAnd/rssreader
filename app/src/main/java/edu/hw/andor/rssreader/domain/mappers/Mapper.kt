package edu.hw.andor.rssreader.domain.mappers

interface Mapper<out OUT, in IN> {
    fun map(input: IN): OUT
}