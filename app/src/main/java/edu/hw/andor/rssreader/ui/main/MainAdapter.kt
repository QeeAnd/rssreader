package edu.hw.andor.rssreader.ui.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import edu.hw.andor.rssreader.R
import edu.hw.andor.rssreader.domain.entities.ChannelUiModel
import edu.hw.andor.rssreader.ui.detail.DetailActivity
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity

class MainAdapter(private val ctx: Context) : RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    private var channels: List<ChannelUiModel> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MainViewHolder(MainItemUI().createView(AnkoContext.create(parent.context)))

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) = holder.bind(channels[position])

    override fun getItemCount() = channels.size

    fun swap(channels: List<ChannelUiModel>) {
        this.channels = channels
        notifyDataSetChanged()
    }

    inner class MainViewHolder(private val mView: View) : RecyclerView.ViewHolder(mView) {
        private val cnt: TextView = mView.find(R.id.cnt)
        private val chn: TextView = mView.find(R.id.chn)
        var linkKey: String = ""
        fun bind(channel: ChannelUiModel) {
            cnt.text = "${channel.count}"
            chn.text = "${channel.title}: ${channel.description}"
            linkKey = channel.linkKey
            mView.onClick {
                ctx.startActivity<DetailActivity>(DetailActivity.CHANNEL_KEY to channel.linkKey, DetailActivity.CHANNEL_NAME to channel.title)
            }
        }
    }
}
