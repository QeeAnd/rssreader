package edu.hw.andor.rssreader.domain.mappers

import edu.hw.andor.rssreader.api.model.Feed
import edu.hw.andor.rssreader.domain.entities.Channel

class ChannelMapper : Mapper<Channel, Feed> {
    override fun map(input: Feed) =
            Channel(input.url ?: "",
                    input.channel?.title ?: "",
                    input.channel?.description ?: "")
}
