package edu.hw.andor.rssreader.ui.main

import android.content.Context
import android.graphics.Color
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import edu.hw.andor.rssreader.R
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class MainItemUI : AnkoComponent<Context> {
    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        cardView {
            lparams(matchParent, dip(50))
            relativeLayout {
                textView("0") {
                    id = R.id.cnt
                    textAppearance = R.style.TextAppearance_AppCompat_Headline
                    textSize = 17f
                    setTextColor(Color.RED)
                }.lparams(dip(27)) {
                    centerVertically()
                    alignParentEnd()
                }
                textView("Channel name ") {
                    id = R.id.chn
                    ellipsize = TextUtils.TruncateAt.END
                    maxLines = 1
                    maxEms = 21
                    textAppearance = R.style.TextAppearance_AppCompat_Body1
                    gravity = Gravity.START
                }.lparams {
                    margin = dip(8)
                    centerVertically()
                    alignParentStart()
                }
            }
        }
    }
}