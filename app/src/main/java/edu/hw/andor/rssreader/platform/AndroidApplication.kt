package edu.hw.andor.rssreader.platform

import android.app.Application
import com.facebook.stetho.Stetho
import edu.hw.andor.rssreader.injection.ApplicationComponent
import edu.hw.andor.rssreader.injection.DaggerApplicationComponent


class AndroidApplication : Application() {

	lateinit var applicationComponent: ApplicationComponent
		private set

	override fun onCreate() {
		super.onCreate()
		Stetho.initializeWithDefaults(this)

		applicationComponent = DaggerApplicationComponent.builder()
				.applicationModule(ApplicationModule(this))
				.build()
	}
}
