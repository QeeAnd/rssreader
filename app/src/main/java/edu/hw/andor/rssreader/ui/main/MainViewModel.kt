package edu.hw.andor.rssreader.ui.main

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import edu.hw.andor.rssreader.domain.entities.ChannelUiModel
import edu.hw.andor.rssreader.platform.AndroidApplication
import edu.hw.andor.rssreader.repository.FeedRepository
import edu.hw.andor.rssreader.repository.NetworkError
import javax.inject.Inject

data class MainViewModel(private val app: Application) : AndroidViewModel(app) {

    @Inject
    lateinit var repository: FeedRepository

    val channels: LiveData<List<ChannelUiModel>>
    val errors: LiveData<NetworkError>

    init {
        (app as AndroidApplication).applicationComponent.inject(this)
        channels = repository.getChannels()
        errors = repository.getErrors()
    }

    fun refreshItems() = repository.refreshItems()

    fun addChannel(channelLink: String) = repository.addChannel(channelLink)

    fun deleteChannel(channelLink: String) = repository.deleteChannel(channelLink)
}
