package edu.hw.andor.rssreader.domain.mappers

import edu.hw.andor.rssreader.api.model.Feed
import edu.hw.andor.rssreader.domain.entities.Item
import java.text.SimpleDateFormat
import java.util.*

class ItemsMapper : Mapper<List<Item>, Feed> {
    companion object {
        //Mon, 05 Jun 2017 07:41:40 +0000
        val format = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH)
    }

    override fun map(input: Feed): List<Item> {
        val items = mutableListOf<Item>()
        val feedItems = input.channel?.feedItems ?: emptyList()
        feedItems.mapTo(items) {
            Item(
                    channelKey = input.url ?: "",
                    pubDate = format.parse(it.pubDate ?: ""),
                    title = it.title ?: "",
                    link = it.link ?: "",
                    description = it.description ?: "",
                    read = 0
            )
        }
        return items
    }
}