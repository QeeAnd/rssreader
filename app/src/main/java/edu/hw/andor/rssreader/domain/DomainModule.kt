package edu.hw.andor.rssreader.domain

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import edu.hw.andor.rssreader.domain.db.ChannelsDao
import edu.hw.andor.rssreader.domain.db.FeedDb
import edu.hw.andor.rssreader.domain.db.ItemsDao
import edu.hw.andor.rssreader.repository.ErrorLiveData
import edu.hw.andor.rssreader.repository.FeedRepository
import edu.hw.andor.rssreader.repository.RefreshUseCase
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Singleton

@Module
class DomainModule {

    @Singleton
    @Provides
    fun provideFeedDb(application: Context) =
            Room.databaseBuilder(application, FeedDb::class.java, "feed.db")
                    .allowMainThreadQueries()
                    .build()

    @Singleton
    @Provides
    fun provideChannelsDao(db: FeedDb) = db.channelsDao()

    @Singleton
    @Provides
    fun provideItemsDao(db: FeedDb) = db.itemsDao()

    @Singleton
    @Provides
    fun provideExecutor(): Executor = Executors.newSingleThreadExecutor()

    @Singleton
    @Provides
    fun provideRepository(itemsDao: ItemsDao,
                          channelsDao: ChannelsDao,
                          refreshUseCase: RefreshUseCase,
                          errorData: ErrorLiveData) =
            FeedRepository(itemsDao, channelsDao, refreshUseCase, errorData)

}