package edu.hw.andor.rssreader.api

import android.content.Context
import com.facebook.stetho.okhttp3.StethoInterceptor
import dagger.Module
import dagger.Provides
import edu.hw.andor.rssreader.BuildConfig
import edu.hw.andor.rssreader.platform.log
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.io.File
import java.util.concurrent.TimeUnit

@Module
class ApiModule {

	private val cacheSize: Long = 10 * 1024 * 1024
	private val cacheTimeSec = 30

	private val cacheInterceptor: Interceptor
		get() = Interceptor {
			val response = it.proceed(it.request())
			val cacheControl = CacheControl.Builder()
					.maxAge(cacheTimeSec, TimeUnit.SECONDS)
					.build()

			response.newBuilder()
					.header("Cache-Control", cacheControl.toString())
					.build()
		}

	@Provides
	fun provideLoggingInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { it.log("Retrofit") })
			.apply { level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE }

	@Provides
	fun provideOkHttpClient(context: Context, loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
		val cache = Cache(File(context.cacheDir, "http-cache"), cacheSize)
		return OkHttpClient.Builder()
				.addInterceptor(loggingInterceptor)
				.addInterceptor(cacheInterceptor)
				.addNetworkInterceptor(StethoInterceptor())
				.cache(cache)
				.build()
	}
}
